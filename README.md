# Jenkins Docker

## Add SSh-Key

generate ssh key pair with `ssh-keygen`.

configure the environment variable `JENKINS_PRIVATE_SSH_KEYDIR` with the location of the private key

Add the public key to `.ssh/authorized_keys` of each slave computer


## Help

- [How to Run Jenkins on Docker](https://www.youtube.com/watch?v=QNZNfvrFBMo&t=402s) for CloudBeesTV
- [Run Jenkins in Docker Container - Jenkins Pipeline Tutorial for Beginners 1-4](https://www.youtube.com/watch?v=pMO26j2OUME)
- [JENKINS im Docker Container installieren und ausführen](https://www.youtube.com/watch?v=fqC7a7jFkug&list=PLypq6k6iOuasC8daGVtjCg_xpBHe39Wd3&index=3)


## To do

- [add selenium container for Junit test](https://www.youtube.com/watch?v=yBn7fiD7u2A&list=PLypq6k6iOuasC8daGVtjCg_xpBHe39Wd3&index=8)
